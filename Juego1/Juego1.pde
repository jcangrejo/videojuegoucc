import processing.sound.*;
import processing.video.*;
Movie myMovie;

import processing.serial.*;
SoundFile file;
Serial myPort;

Puntaje puntaje;
Ambiente ambiente;
Personaje personaje;

PImage fondo;
PImage pj;
PImage puntajeImage;
PImage plant;
PImage tubo;


boolean inGame=true;// variable para verificar si no ha perdido
boolean paso =false;
boolean accionRecoge=false;
boolean spawn;
int flagVideo= 0;
float esperaSalto=0;
int level;

int cordx=0;
float cordy=119;
int velocidad = 7;
int actionDown=int (cordy);
boolean salto =false;// variable para hacer el cambio de estado del salto
boolean actionOn=false; //valida si se activó la acción
boolean rotatePerson = true;
int scoreTime=0;


void setup() {
  myMovie = new Movie(this, "intro.mp4");
  level=1;
  ambiente = new Ambiente();
  myMovie.loop();  
  puntaje = new Puntaje(0,0);
  scoreTime=0;
  
  size(700,400);
  background(0);
  fondo = loadImage("fondo_1.jpg");
  puntajeImage = loadImage("puntaje.png");
  tubo= loadImage("tuboW.png");
  
  
  pj = loadImage("cp.png");
  //file = new SoundFile(this, "soundtrack.mp3");
  //file.play();
  
  spawn=false;
  
   
}


void draw() {
  scoreTime++;
  
  if(flagVideo==0){
  estado1();
  }
  else if(inGame==true){
    estado2();    
    estado0();
    
  }
 
}

void movieEvent(Movie m){//ejecución del video
  m.read();
}

void estado1(){
  textSize(32);
  text("Apachurra una tecla", 200, 300); 
  image(myMovie, 0,0,700,400);
 if(flagVideo==0){
   myMovie.loop();
     
 }
}


void estado2(){
  myMovie.stop();
}

void estado0 ()
{    
  int obst1=0;
  //println("obst1 "+obst1);
 
  if(paso){
    fondo = loadImage("fondo_2.jpg");
    cordx= 0;
    image(fondo, cordx, 0);
    paso=false;
  }
  image(fondo, cordx, 0, 4000, 400);
  if(obst1==0){
    //println("IF OBSTACULO");
    //obst1= ambiente.crearObstaculo();
    ambiente.agua(int(random(200,500)));
    //ambiente.minerales();
  }
  obst1=1;
  
  image(pj, 120, cordy);
  image(puntajeImage, 110,0);
  
  if(salto && esperaSalto<22 ){
     
     delay(20);
     cordy = cordy-9.8;
     delay(8);
     esperaSalto=esperaSalto+0.6;
    
    if(esperaSalto>=10){
      salto=false;
      esperaSalto=0;
      pj = loadImage("cp.png");
    }
  } 
  
  if(salto==false && cordy <119){
     delay(20);
     cordy = cordy+6.5;
     delay(8);
  }
  
  //validación de salto
  if(level==1){
    validarSaltoLevel1();
  }  
  else if(level==2){
    validarSaltoLevel2();
  }
  recogerMineral();
  
  
  if(inGame==true){
    textSize(18);
    text(scoreTime, 175, 20);
    if(level==1){
      textSize(12);
      text("Agua > 1\nNitrogeno >2", 550, 20);
  
    }
   }
   
}

  void engGameFail(){
    fondo=loadImage("gameOverFail.png");
    textSize(32);
    text("GAME OVER", 200, 300);
    image(fondo,0, 0, 700,400);
    redraw();
    println("perdio");
    inGame=false;
  }
  
  void recogerMineral(){
     //validarAcción 
     if(actionOn && actionDown<200){
       //println("actionDown INICIA");
       actionDown=actionDown+3;
       insertarTubo();   
       if(actionDown>=199){
         actionOn=false;
         //println("actionDown FINALIZA");
       }
     }
     else if(actionOn==false && actionDown>119  && salto==false){
       actionDown=actionDown-3;
       insertarTubo();
       //println("actionDown --");
     }
     //println("actionDown", actionDown);
    
    //------------- 
  }
  
  void insertarTubo(){
     image(tubo,120,actionDown,100,120); 
  }
  
  
  
void validarSaltoLevel1(){
  if(cordy>70){
      if(cordx<-63 && cordx>-196){
        engGameFail();
      }
      else if(cordx<-406 && cordx>-553){
        engGameFail();        
      }
      
      else if(cordx<-672 && cordx>-819){
        engGameFail();        
      }
      
      else if(cordx<-966 && cordx>-1085){
        engGameFail();        
      }
      
      else if(cordx<-1316 && cordx>-1442){
        engGameFail();        
      }
      
      else if(cordx<-1596 && cordx>-1722){
        engGameFail();        
      }

      else if(cordx<-1897 && cordx>-2030){
        engGameFail();        
      }
      
      else if(cordx<-2261 && cordx>-2387){
        engGameFail();        
      }
      
      else if(cordx<-2632 && cordx>-2758){
        engGameFail();        
      }
      
      else if(cordx<-2919 && cordx>-3045){
        engGameFail();        
      }
      
   }
}

void validarSaltoLevel2(){
  if(cordy>70){
      if(cordx<-301 && cordx>-413){
        engGameFail();
      }
      else if(cordx<-798 && cordx>-903){
        engGameFail();        
      }
      
      else if(cordx<-1113 && cordx>-1211){
        engGameFail();        
      }
      
      else if(cordx<-1393 && cordx>-1498){
        engGameFail();        
      }
      
      else if(cordx<-1890 && cordx>-2009){
        engGameFail();        
      }
      
      else if(cordx<-2345 && cordx>-2457){
        engGameFail();        
      }

      else if(cordx<-2800 && cordx>-2912){
        engGameFail();        
      }
      
   }
}


void cambioPersonaje(){
    if(!rotatePerson){
        pj = loadImage("cp.png");
        rotatePerson = !rotatePerson;
     }else{
        pj = loadImage("cp_paso.png");
        rotatePerson = !rotatePerson;
    } 
}





void keyPressed() {
  
  if(flagVideo==0){
      flagVideo=1;
    }
  
  println(cordx, "   en y  ", cordy);
  if(cordx <=-3272){    
    paso=true;
    level++;
  }
  
  
  if (key == CODED) {
    if (keyCode == LEFT && actionOn==false && actionDown <=119) {
      if(cordx <=-7){
        cordx = cordx+velocidad;
        
        accionRecoge=false;
        cambioPersonaje();
        
        
    }
      
    } else if (keyCode == RIGHT  && actionOn==false && actionDown <=119) {      
      cordx = cordx-velocidad;
      cambioPersonaje();
      accionRecoge=false;
      
    } 
    else if(keyCode == UP && salto==false && cordy>=119  && actionOn==false && actionDown <=119){
          salto=true;
          pj = loadImage("cp_salto.png");
    }
    
   }
   
   else if(key == 32 && actionDown >= 119 && accionRecoge==false){
     text("Tomando minerales", 200, 300);
     actionOn=true;
     accionRecoge=true;
   }
   
   //reniciar juego
   else if(key == 84 || key == 114){
     inGame=true;
     cordx=0;
     setup();
     redraw();
   }
}
